import unittest

from nmigen import *

from axi_burster import AxiBurster
from lib.bus.axi.axi_endpoint import AxiEndpoint, Response, BurstType
from util.sim import SimPlatform

class TestSimAxiBurster(unittest.TestCase):
    def test_main(self):
        axi_in = AxiEndpoint(addr_bits=32, data_bits=64, master=False, lite=False, id_bits=12)
        dut = AxiBurster(axi_in, 4)
        axi_out = dut.output
        def testbench():
            pass
            # To be defined
        def dummy_process():
            for i in range(10):
                yield

        platform = SimPlatform()
        platform.add_sim_clock("sync", 200e6)
        platform.add_process(dummy_process, "sync")
        platform.sim(dut, traces = [
            dut.input.read_address.valid,
            dut.input.read_address.ready,
            dut.input.read_address.payload,
            dut.fill_level,
            dut.output.read_address.payload,
            dut.output.read_address.valid,
            dut.output.read_address.ready,
            dut.output.read_address.burst_len] )

if __name__ == "__main__":
    thing = TestSimAxiBurster()
    thing.test_main()
