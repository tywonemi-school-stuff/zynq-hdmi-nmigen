from nmigen import *
from lib.bus.stream.stream import BasicStream
from lib.bus.axi.axi_endpoint import AxiEndpoint, Response, BurstType

class AxiBurster(Elaboratable):
    def __init__(self, input:AxiEndpoint, burst_length):
        self.input = input
        self.output = AxiEndpoint.like(input, name="burster_out")
        self.incremental = Signal()
        self.fill_level = Signal(range(burst_length))
        self.burst_length = burst_length
        # reset values?
    def elaborate(self, platform):
        m = Module()

        # Combinatorically connect data lines
        m.d.comb += self.input.read_data.ready.eq(self.output.read_data.ready)
        m.d.comb += self.output.read_data.valid.eq(self.input.read_data.valid)
        m.d.comb += self.output.read_data.payload.eq(self.input.read_data.payload)
        # m.d.comb += self.output.read_data.id.eq(self.input.id.value)
        # m.d.comb += self.output.read_data.last.eq(self.input.last.value)
        # m.d.comb += self.output.read_data.reset.eq(self.input.last.value)

        with m.If(self.output.read_address.ready & self.output.read_address.valid):
            m.d.sync += self.output.read_address.valid.eq(0)
            m.d.sync += self.input.read_address.ready.eq(1)

        with m.If(self.input.read_address.ready & self.input.read_address.valid):
            with m.If((~self.incremental) | (self.fill_level == 0)):
                m.d.sync += self.output.read_address.payload.eq(self.input.read_address.payload)
            with m.If((~self.incremental) | (self.fill_level == self.burst_length-1)):
                m.d.sync += self.output.read_address.valid.eq(1)
                m.d.sync += self.input.read_address.ready.eq(0)
                m.d.sync += self.output.read_address.burst_len.eq(self.fill_level)
                m.d.sync += self.fill_level.eq(0)
                # present first address
        return m
