# Test HDMI using a given modeline
# Only slightly modified from experiments/hdmi_test.py
from nmigen import *

from cores.hdmi.hdmi import Hdmi
from cores.hdmi.cvt import generate_modeline
from devices import PynqZ2Platform
from soc.cli import cli
from soc.platforms.zynq import ZynqSocPlatform
from cores.csr_bank.csr_bank import ControlSignal

class Top(Elaboratable):
    def elaborate(self, platform: ZynqSocPlatform):
        m = Module()
        self.test_register = test_register = ControlSignal(32)
        m.d.comb += test_register.eq(0xDEADBEEF)

        hdmi_plugin = platform.request("hdmi", "out")
        m.submodules.hdmi = Hdmi(hdmi_plugin, modeline='Modeline "Mode 3" 74.250 1280 1390 1420 1650 720 725 730 750 +hsync +vsync')

        return m


if __name__ == "__main__":
    with cli(Top, runs_on=(PynqZ2Platform, ), possible_socs=(ZynqSocPlatform, )) as platform:
        pass