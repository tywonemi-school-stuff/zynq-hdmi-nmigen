from nmigen.build import *
from nmigen.vendor.xilinx_7series import *

__all__ = ["PynqZ2Platform"]


class PynqZ2Platform(Xilinx7SeriesPlatform):
    device = "xc7z020"
    package = "clg400"
    speed = "1"
    resources = [
        Resource("hdmi", "out",
             # high speed serial lanes
             Subsignal("clock",  DiffPairs("L16", "L17", dir='o'), Attrs(IOSTANDARD="TMDS_33")),
             Subsignal("data", DiffPairs("K17 K19 J18", "K18 J19 H18", dir='o'), Attrs(IOSTANDARD="TMDS_33")),
             Subsignal("hpd",    Pins     ("T19", dir='i'),        Attrs(IOSTANDARD="LVCMOS25")), # hot plug detect
        ),
        Resource("hdmi", "in",
             # high speed serial lanes
             Subsignal("clock",  DiffPairs("N18", "P19", dir='i'), Attrs(IOSTANDARD="TMDS_33")),
             Subsignal("data", DiffPairs("V20 T20 N20", "W20 U20 P20", dir='i'), Attrs(IOSTANDARD="TMDS_33")),
        )
    ]
    connectors = []
