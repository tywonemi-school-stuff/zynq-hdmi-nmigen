#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

void *memset64(void *m, void *val, size_t count);
void *memset64(void *m, void *val, size_t count)
{
    long long int *buf = m;

    while(count--)
    {
	memcpy(buf, val, 4);
        buf++;
    }
    return m;
}

static void deadbeef(int offset);
static void hdmitest(void);
static void deadbeef(int offset) {
  int fd = open("/dev/mem", O_RDWR);
  printf("fd is %d\n", fd);
  void* ptr = mmap(NULL, (size_t) 8192U, PROT_READ|PROT_WRITE,
                 MAP_SHARED, fd, 0x40000000);
  close(fd);
  //return;
  if(ptr == MAP_FAILED)
  {
    printf("mmap failed\n");
    return;
  }
  volatile unsigned* reg = (unsigned*)(ptr+offset);
  printf("Register 0x%08X\n:", (unsigned int)(0x40000000+offset));
  printf("\t0x%08X\n", (unsigned int)*reg);
  munmap(ptr, (size_t)8192U);

}

#define CURRBUFF 0x40000000
#define BUFFSIZE 0x00200000
//#define BUFFSIZE 0x00900000
#define BUFFBASE 0x3B000000
#define BUFFNUM  4U
//#define BUFFNUM 1U
#define TEST_VAL 0xFF00FF33
static void writepx(void* pbuf, int x, int y, int rgb);
static void writepx(void* pbuf, int x, int y, int rgb) {
  *(int *)(((int)pbuf) + 8 * x + 10240 * y) = rgb;
}
static void hdmitest (void) {
  int fd = open("/dev/mem", O_RDWR);
  // printf("fd is %d", fd);
  printf("running mapped hdmitest()\n");
  void* pcur = mmap(NULL, 8192, PROT_READ|PROT_WRITE,
                 MAP_SHARED, fd, CURRBUFF);
  printf("mmaped current buffer register\n");
  void* pbuf = mmap(NULL, BUFFSIZE*BUFFNUM, PROT_READ|PROT_WRITE,
                 MAP_SHARED, fd, BUFFBASE);
  printf("mmaped buffer\n");
  close(fd);
  printf("closed fd\n");
  if((pbuf == MAP_FAILED) || (pcur == MAP_FAILED))
  {
    printf("mmap failed\n");
    return;
  }

  *((int*)pcur) = 0;
  //struct timespec start, stop;
  void * pw = pbuf + (0x600000/sizeof(void));
  //for (int i = 0; i < 600; i++) {
    /* For many frames, do: */
    unsigned long long int val = 0x777777;
    memset64(pw, (void *)&val, BUFFSIZE/8);
    //writepx(pw, 10,10, 0xFFFFFF);
    //writepx(pw, 0, 10, 0x00FFFF);
    //writepx(pw, 10, 0, 0xFFFF00);
    writepx(pw, 1,  1, 0x000000);

    //double took = (stop.tv_sec - start.tv_sec) + (stop.tv_nsec - start.tv_nsec) / 1e9;
    //printf("wrote %f\n and took %f\n", ((float)BUFFSIZE), took);
    //printf("write speed %f MB/s\n", ((float)BUFFSIZE)/(1024*1024*took));
  //}

  munmap(pbuf, BUFFSIZE*BUFFNUM);
  munmap(pcur, 8192);
}

int main() {
  deadbeef(0x00);
  deadbeef(0x04);
  hdmitest();
  return 0;
}
