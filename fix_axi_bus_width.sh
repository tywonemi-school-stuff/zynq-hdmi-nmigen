# set the bit width of all axi hp slaves to 64 bits
devmem2 0xf8008000 w 0
devmem2 0xf8008014 w 0xF00
devmem2 0xf8009000 w 0
devmem2 0xf8009014 w 0xF00
devmem2 0xf800a000 w 0
devmem2 0xf800a014 w 0xF00
devmem2 0xf800b000 w 0
devmem2 0xf800b014 w 0xF00
