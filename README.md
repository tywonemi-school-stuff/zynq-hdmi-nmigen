# KRP semestral project plan

## TODO: verify files in this repo are up to date with project directory!

## The plan
This project is primarily for educational purposes. The goal is to create a functional ethernet-attached(?) HDMI capture card or HDMI source with as few proprietary tools used as possible.

## In this repo

+ modified_hdmi_test.py is a modified version of hdmi_test.py and camera.py [from apertus open cinema's nmigen rewrite of litevideo](https://github.com/apertus-open-source-cinema/nmigen-gateware)
+ default_hdmi_test.py is a slightly modified version of hdmi_test.py, with my monitor's 720p modeline, and Pynq Z2.
+ pynq_z2_platform.py is a platform definition file, and has to be placed into 
+ simplefb.ko is a simple framebuffer (in-tree) kernel module cross-compiled the Pynq Z2 linux image, yet incompatible
+ bitstream_to_PL.py loads the bitstream
+ set_clocks.py sets clock frequencies for the programmable logic


## Technical solution
### Hardware
[Pynq-Z2](http://www.tul.com.tw/ProductsPYNQ-Z2.html), [lowest price is on seeed](https://www.seeedstudio.com/PYNQ-Z2-board-based-on-Xilinx-Zynq-C7Z020-SoC-p-2835.html). This is a board with a System on Chip with a plentiful FPGA, and a dual-core chip similar in performance to Raspberry Pi 1. Ideal for network-attached application specific processor based accelerators. Similar parts have been used in AntMiner BTC miners.


### How to build
All tools used are cross-platform

+ install Vivado. Warning: this is a massive download. Test your installation by typing `vivado -version` in command line. Watch out: new versions of ubuntu are unsupported but [functional with minor fixes](https://forums.xilinx.com/t5/AI-and-Vitis-AI/vivado-2019-2-automatically-exiting-on-ubuntu-20-04/td-p/1100334).
+ install python3
+ install nmigen (from [whitequark's repo](https://github.com/nmigen/nmigen/), m-labs repo is obsolete)
+ install yosys, not from apt, but from [git](https://github.com/YosysHQ/yosys) from source, the latest release is over a year old. Get the [Axiom Micro gateware](https://github.com/apertus-open-source-cinema/nmigen-gateware) project
+ locate yourself in nmigen-gateware/src and copy to this directory your top-level source file, in this case, the modified version Experiments/hdmi_framebuffer.py
+ run `python3 .\hdmi_framebuffer.py -bp -d PynqZ2 -s Zynq`. This translates nmigen to verilog with yosys, creates a vivado TCL build script, and runs it, creating a bitstream

### About nMigen
nMigen stands for new Migen. Migen was a modern HDL which parsed Python3 syntactic trees, which limited dynamic HDL generation. Instead, nMigen code is executed directly with a Python3 interpreter, the emission of other HDL or intermediate formats is handled with some boilerplating. It is an HDL, not high level synthesis. There is a focus on flexibility and its Python3 source is very concisely and idiomatically written. Its syntax isn't quite as elegant as other modern flexible HDLs like [Chisel](https://github.com/schoeberl/chisel-examples/blob/master/src/main/scala/uart/Uart.scala) or [Spinal](https://spinalhdl.github.io/SpinalDoc-RTD/SpinalHDL/Examples/Simple%20ones/counter_with_clear.html).

### About HDMI
HDMI is a huge standard with a small minimal subset, which is basically equivalent to digital DVI over a different physical interface. That's what we are doing here. Red, Green, Blue each get their own TMDS (basically properietary encoded serial over LVDS) pair. The pixel clock also gets a pair. The bits aren't asserted in sync with the pixel clock, since ten bits ([8 bits encoded to 10 bits](https://en.wikipedia.org/wiki/8b/10b_encoding) as per TMDS spec to remove DC offset and maximize Hamming distance) have to be transmitted per frame on each of the three color lanes. So pixels are asserted in sync with 10x the pixel clock. This is called the TMDS clock and this is the true frequency that has to be able to propagate well over the physical layer. Implementing HDMI output on FPGA typically (including in the litevideo rewrite used here) has logic running at the pixel clock, pushing parallel 10bit registers into a 10:1 serializer (SERDES).

It also has an I2C bus, called the DDC (display data channel). Among its capabilities it the transmission of EDID (Extended Display Identification Data). The HDMI source is the I2C master and requests from the sink its supported modelines, that is, the specification of resolution, framerate, and timing. It can receive multiple supported modelines and has to pick one of these. At the moment, this is not implemented in the HDMI core in Axiom micro gateware. To use the bitstream here to generate video output for your monitor, you have to find the modeline by plugging the monitor into a linux computer with a graphics card, picking a compatible modeline, and pasting it into the modeline argument of the Hdmi module initialization. On Debian-based Linux:

```sh
sudo apt install read-edid
# With monitor disconnected:
sudo get-edid
# With monitor connected:
sudo get-edid
# Compare the outputs, see what EDID bus has been detected. In my case, the XPS 15's HDMI output had bus 10
# Replace 10 your bus number:
sudo get-edid -b 10 > edid
parse-edid < edid
# These are the modelines for your monitor. Pick one and use it in your nmigen.
```

### About Zynq
Zynq 7000 is a Xilinx product series of system-on-chips consisting of 1-2 ARM Cortex-A9 cores and FPGA. Note that the hard logic part with the processor is called processing system (PS) and the FPGA part is called programmable logic (PL). This combines the flexibility and ease of use of microprocessors with the determinism and parallelized processing capabilities of FPGA. This project isn't the best use of a Zynq, since a cheaper Raspberry Pi could perform the same thing out of the box without programmable logic. The best use of Zynq is to prototype SoCs without programmable logic, or to implement high bandwidth / reliability custom logic that's never going to be fabricated at scale to make ASICs more financially viable (scientific data processing, space hardware for event timing and communications, high frequency trading). Zynq uses the AXI bus with a configurabe

### About AXI
AXI is a proprietary parallel bus architecture by ARM. Its bit width is left to the implementation (64-bit on Zynq 7000, configurable to other values). The write and read channels are separate. A bus starts at one or more source (master) endpoints and ends at one or more sink (slave) endpoints. All read and write operations are addressed, as in, one sink endpoint provides access to typically many registers. Data is always sent on the first period, when the master signals VALID data, and the slave signals it is READY to receive data; this process is called the handshake. To reduce overhead, one handshake and addressing can be used for many bus words being transmitted in sequence by using a burst transaction. With multiple masters, like in a multi-processor or processor + DMAs setup, read modify write operation data integrity is ensured with exclusive bus accesses. 

### AXI modifications
+ no addressing = AXI stream 
+ no bursts, no cached memory support = AXI lite
+ added cache coherency operations = ACE

### Using the framebuffer
The simplest possible way to draw things in a memory-mapped framebuffer is to provide a device tree binding. The device tree is a configuration system, informing the linux kernel about the availability of hardware. Devices described in nodes of the device tree can then be accessible to programs as device files. Device tree overlays can be loaded and unloaded dynamically while the system runs. Device tree source files have to be compiled into device tree blobs (.dtbo) before being loaded. The fatbitstream system for the axiom micro relies on blobs being placed in `/sys/kernel/config/device-tree/overlays/$OVERLAYNAME/dtbo`, which is also present in the PYNQ linux distribution, and the pynq python wrappers provide their own methods for doing this at bitstream loading time.

When a device tree fragment is loaded, the driver it is compatible with should automatically load as well. In this case, the driver needed is the [simple framebuffer](https://www.kernel.org/doc/Documentation/devicetree/bindings/display/simple-framebuffer.txt), an in-tree linux kernel driver, typically used only when a linux system is booting to draw boot log on an external screen. Pynq images are not shippined with this kernel built into them or as a kernel module. We can see that it is not available by listing the kernel configuration with `zcat /proc/config.gz | grep -v '^#' | grep FB` and seeing that neither of `CONFIG_FB_SIMPLE=m` (build as module) or `CONFIG_FB_SIMPLE=y` (build statically) is present. One can build a missing module on its own through one of two ways:

+ get the module and dependencies' source files, and the linux-source and linux-headers packages, and compile it on the target machine to ensure compatibility
+ cross-compile on any machine from source

Attempting the second option, I successfully built simplefb.ko by getting the armhf GCC toolchain and xilinx' linux kernel release that zynq was using. However, the system rejected this kernel module. `insmode simplefb.ko` returned `insmod: ERROR: could not insert module ./simplefb.ko: Invalid module format`. It is possible that this is due to a mismatch in kernel build configurations.

Attempting the first option, I found that the header and sources packages as provided in this distribution don't have functional paths and some of the sources are actually for x86. It seems that the best solution would be to rebuild the entire pynq image, however, this requires familiarity with petalinux and yocto. Porting axiom-firmware onto the Pynq seems like a sane option, but figuring out and modifying u-boot configuration contained in flash and on SD card for the Pynq also requires building petalinux; the pynq repository does not provide full device tree source, it has its own configuration system which patches common petalinux files with platform-dependent, and the repo provides only the platform-dependent.

Another issue is that in the axiom micro setup, the framebuffer is at a hard-coded location in RAM, and it is expected that at boot time, the [u-boot bootloader](https://github.com/u-boot/u-boot) is told to reserve memory for the framebuffer using something similar to `fdt rsvmem add 0xf800000 0x08000000;`, to prevent the kernel from accessing it and allocating to it. If it allocates a process to the framebuffer range, when it's written to manually through /dev/mem, the system crashes. This is strange, since if the range is being used by a process, mmap should return MAP_FAILED, which is checked both in my experimental c code, and the devmem2 command-line utility, both of which would crash in this way. Because of this, it is rather unsafe to write to the framebuffer.

## Things I did

### Invalid settings in DDR OSERDES
When I was figuring out the build setup, I went through the ~250 Vivado headless warnings and errors. I traced down one class of them to [incorrect DDR settings](https://github.com/apertus-open-source-cinema/nmigen-gateware/issues/2). The serializers were being configured with an unsupported input-output bit ratio for its tristate interface, as per [the TRM](https://d2m32eurp10079.cloudfront.net/Download/pynqz2_user_manual_v1_0.pdf). Since the tristate interface was not being used, and it worked on others' machines at that point with that warning emitted, it turned out not to be a problem, but I found and tested a correct setting which was [pulled](https://github.com/apertus-open-source-cinema/nmigen-gateware/pull/3) upstream.

### AXI Burster
Once I managed to configure things correctly and see the contents of my constant-filled framebuffer on my screen, once I wrote one single pixel, I could see that it is flickering across multiple lines non-periodically. A hypothesis about hsync and vsync internal signals not being correctly generated turned out to be false. The issue turned out to be caused by the logic requiring one RAM framebuffer word readout per pixel displayed on the output. This was later solved by contributor anuejn by correct data buffering. Seeing the architecture, I noticed all data is read out one word per transaction, likely increasing DDR resource usage. For this reason, I specified a module which accumulates reads into multiple reads 

\includepdf{hdmi_framebuffer.pdf}

### Pynq Z2 hardware support
Adding support for the HDMI resources was as simple as going through the schematic for the dev board and creating Resource objects with the correct pins and inversions for differential pairs. Since the pynq image doesn't have PL clocks exposed in devicetree, I created set_clocks.py using the Pynq python interface. The pynq library takes care of calculating necessary PLL coefficients. You need to read the build log for the frequencies, or find them in the emitted .fatbitstream.sh file. These contain SoC-compatible commands to set the clocks and AXI width and base64-encoded bitstream. Similarly, a bitstream-loading python script is provided. Changing AXI width is not necessary since Pynq images already are at 64-bit.



## Resources
[Get/build a clean Linux kernel / boot image](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841961/Zynq+Linux), alternate docs [on digikey](https://www.digikey.com/eewiki/display/linuxonarm/Zynq-7000). 

[Pynq Z2 reference manual](https://d2m32eurp10079.cloudfront.net/Download/pynqz2_user_manual_v1_0.pdf).

[Python overlay wrapper](https://github.com/Xilinx/PYNQ/blob/82da59069aa56f62dbc7dda484b6ff2f805b60cc/pynq/lib/video/xilinx_hdmi.py)