# Test HDMI using a given modeline

from nmigen import *

# from cores import *
from cores.hdmi.hdmi import Hdmi
from devices import PynqZ2Platform
from soc.platforms.zynq import ZynqSocPlatform
from cores.axi.buffer_writer import AxiBufferWriter
from cores.ring_buffer_address_storage import RingBufferAddressStorage
from cores.hdmi.hdmi_buffer_reader import HdmiBufferReader
from soc.cli import cli

class Top(Elaboratable):
    def elaborate(self, platform: ZynqSocPlatform):
        m = Module()
        
        ring_buffer = RingBufferAddressStorage(buffer_size=0x1200000, n=4)

        hdmi_resource = platform.request("hdmi", "out")
        m.submodules.hdmi = HdmiBufferReader(
            ring_buffer, hdmi_resource,
            modeline='Modeline "Mode 1" 148.500 1920 2008 2052 2200 1080 1084 1089 1125 +hsync +vsync'
        )
        # Publish current write buffer. In other examples, this is handled in AxiBufferWriter
        self.current_write_buffer = ring_buffer.current_write_buffer

        return m


if __name__ == "__main__":
    with cli(Top, runs_on=(PynqZ2Platform, )) as platform:
        pass
